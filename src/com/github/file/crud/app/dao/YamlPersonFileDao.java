package com.github.file.crud.app.dao;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.converter.YamlConverter;
import com.github.file.crud.app.exceptions.CrudException;
import com.github.file.crud.app.models.Person;
import com.github.file.crud.app.utils.FileUtil;

import java.io.File;
import java.util.List;

public class YamlPersonFileDao implements PersonFileDao {

    protected final Converter converter = new YamlConverter();

    @Override
    public boolean create(String fullPathToFile, Person person) {
        if (FileUtil.isFileExist(fullPathToFile)) {
            List<Person> existPersons = read(fullPathToFile);

            Person foundPerson = findPersonByIdInPersonList(person.getId(), existPersons);
            if (foundPerson != null) {
                throw new CrudException("Can not add person. Person with this id already exist in this file!");
            }

            existPersons.add(person);
            return saveDataToFile(fullPathToFile, existPersons);
        } else {
            File fileWithPersons = FileUtil.createFile(fullPathToFile);
            return saveDataToFile(fileWithPersons, person);
        }
    }

    @Override
    public Person read(String fullPathToFile, long id) {
        String stringDataFromFile = FileUtil.readFileAsStringData(fullPathToFile);
        List<Person> persons = converter.fromStringDataToObjects(stringDataFromFile);

        return findPersonByIdInPersonList(id, persons);
    }

    @Override
    public List<Person> read(String fullPathToFile) {
        String stringDataFromFile = FileUtil.readFileAsStringData(fullPathToFile);
        return converter.fromStringDataToObjects(stringDataFromFile);
    }

    @Override
    public boolean update(String fullPathToFile, Person person) {
        if (FileUtil.isFileExist(fullPathToFile)) {
            List<Person> existPersons = read(fullPathToFile);

            Person foundPerson = findPersonByIdInPersonList(person.getId(), existPersons);
            if (foundPerson == null) {
                throw new CrudException("Can not edit person! Person does not exist in this file!");
            }

            setNewPersonParams(foundPerson, person);
            return saveDataToFile(fullPathToFile, existPersons);
        } else {
            throw new CrudException("File does not exist!");
        }
    }

    @Override
    public boolean delete(String fullPathToFile, long personId) {
        if (FileUtil.isFileExist(fullPathToFile)) {
            List<Person> existPersons = read(fullPathToFile);

            int foundPersonIndex = -1;

            for (int i = 0; i < existPersons.size(); i++) {
                if (existPersons.get(i).getId()==personId) {
                    foundPersonIndex = i;
                    break;
                }
            }

            if (foundPersonIndex == -1) {
                throw new CrudException("Can not delete person! Person does not exist in this file!");
            }

            existPersons.remove(foundPersonIndex);
            return saveDataToFile(fullPathToFile, existPersons);
        } else {
            throw new CrudException("File does not exist!");
        }
    }

    protected void setNewPersonParams(Person oldPerson, Person newPerson) {
        oldPerson.setFirstName(newPerson.getFirstName());
        oldPerson.setLastName(newPerson.getLastName());
        oldPerson.setAge(newPerson.getAge());
        oldPerson.setCity(newPerson.getCity());
    }

    protected Person findPersonByIdInPersonList(long personId, List<Person> existPersons) {
        Person foundPerson = null;
        for (Person existPerson : existPersons) {
            if (existPerson.getId() == personId) {
                foundPerson = existPerson;
                break;
            }
        }
        return foundPerson;
    }

    protected boolean saveDataToFile(String fullPathToFile, List<Person> dataToSave) {
        String personsAsStringData = converter.toStringDataFromObjects(dataToSave);
        return FileUtil.writeStringDataToFile(fullPathToFile, personsAsStringData);
    }

    protected boolean saveDataToFile(File file, Person dataToSave) {
        String personAsStringData = converter.toStringDataFromObject(dataToSave);
        return FileUtil.writeStringDataToFile(file, personAsStringData);
    }
}
