package com.github.file.crud.app;

import com.github.file.crud.app.view.ConsoleMenuCrudApplication;
import com.github.file.crud.app.view.MenuCrudApplication;

public class CrudApplication {

    public static void main(String[] args) {
        CrudApplication.start();
    }

    public static void start() {
        System.out.println("=== [Welcome to CRUD application] ===");
        MenuCrudApplication menu = new ConsoleMenuCrudApplication();
        menu.runMenu();
    }
}
