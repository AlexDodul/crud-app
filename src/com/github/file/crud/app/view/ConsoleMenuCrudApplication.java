package com.github.file.crud.app.view;

import com.github.file.crud.app.exceptions.CrudException;
import com.github.file.crud.app.view.MenuCrudApplication;
import com.github.file.crud.app.view.MenuHelper;
import com.github.file.crud.app.view.models.MenuCommand;

import java.util.Scanner;

import static com.github.file.crud.app.utils.ScannerWrapper.scanner;

public class ConsoleMenuCrudApplication implements MenuCrudApplication {

    @Override
    public void runMenu() {
        MenuHelper.printHelp();
        while (true) {
            System.out.print("\nEnter the command: ");
            try {
                MenuCommand command = MenuHelper.parseMenuCommand(scanner.nextLine());
                MenuHelper.processCommand(command);
            } catch(CrudException e) {
                System.out.println("Your command is incorrect! " + e.getMessage());
                System.out.println("Please try to enter again!");
            }
        }
    }
}
