package com.github.file.crud.app.view.models;

import com.github.file.crud.app.utils.FileType;
import com.github.file.crud.app.utils.MenuOperation;

import java.util.Objects;

public class MenuCommand {

    private MenuOperation operation;
    private String fullPathToFile;
    private FileType fileType;


    public MenuCommand() {
    }

    public MenuCommand(MenuOperation operation, String fullPathToFile, FileType fileType) {
        this.operation = operation;
        this.fullPathToFile = fullPathToFile;
        this.fileType = fileType;
    }

    public MenuOperation getOperation() {
        return operation;
    }

    public void setOperation(MenuOperation operation) {
        this.operation = operation;
    }

    public String getFullPathToFile() {
        return fullPathToFile;
    }

    public void setFullPathToFile(String fullPathToFile) {
        this.fullPathToFile = fullPathToFile;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuCommand that = (MenuCommand) o;
        return operation == that.operation &&
                Objects.equals(fullPathToFile, that.fullPathToFile) &&
                fileType == that.fileType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(operation, fullPathToFile, fileType);
    }
}
