package com.github.file.crud.app.converter;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.models.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlConverter implements Converter {

    @Override
    public List<Person> fromStringDataToObjects(String data) {
        if (data == null || "".equals(data)) {
            return null;
        }
        List<Person> personList = new ArrayList<>();
        Pattern pattern = Pattern.compile("<person>.*?<id>(.*?)</id>.*?<firstName>(.*?)</firstName>.*?<lastName>(.*?)</lastName>.*?<age>(.*?)</age>.*?<city>(.*?)</city>.*?</person>",
                Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(data);
        while (matcher.find()) {
            personList.add(new Person(Long.parseLong(matcher.group(1)), matcher.group(2), matcher.group(3), Integer.parseInt(matcher.group(4)), matcher.group(5)));
        }
        return personList;
    }

    @Override
    public String toStringDataFromObjects(List<Person> people) {
        if (people == null || people.isEmpty()) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
                .append("<people>\n");
        for (Person person : people) {
            result.append("\t")
                    .append(toFormat(person))
                    .append("\n");
        }
        result.append("</people>");
        return result.toString();
    }

    public String toFormat(Person person) {
        return String.format("<person>\n\t\t<id>%d</id>\n\t\t<firstName>%s</firstName>\n\t\t<lastName>%s</lastName>\n\t\t<age>%s</age>\n\t\t<city>%s</city>\n\t</person>",
                person.getId(),
                person.getFirstName(),
                person.getLastName(),
                person.getAge(),
                person.getCity()
        );
    }
}
