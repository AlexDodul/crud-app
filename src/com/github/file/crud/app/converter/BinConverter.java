package com.github.file.crud.app.converter;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.exceptions.CrudException;
import com.github.file.crud.app.models.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.ISO_8859_1;

public class BinConverter implements Converter {

    @Override
    public List<Person> fromStringDataToObjects(String data) {
        byte[] bytes = data.getBytes(ISO_8859_1);

        if (bytes.length == 0) {
            return new ArrayList<>();
        }

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
             ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {
            return (List<Person>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new CrudException("Rejected convert byte data to people' list!");
        }
    }

    @Override
    public String toStringDataFromObjects(List<Person> people) {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
            objectOutputStream.writeObject(people);
            return new String(byteArrayOutputStream.toByteArray(), ISO_8859_1);
        } catch (IOException e) {
            throw new CrudException("Can't deserialize byte data to string!");
        }
    }
}
