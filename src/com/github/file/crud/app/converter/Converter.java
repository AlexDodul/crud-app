package com.github.file.crud.app.converter;

import com.github.file.crud.app.models.Person;

import java.util.ArrayList;
import java.util.List;

public interface Converter {

    List<Person> fromStringDataToObjects(String data);

    String toStringDataFromObjects(List<Person> people);

    default String toStringDataFromObject(Person person) {
        List<Person> people = new ArrayList<>();
        people.add(person);
        return toStringDataFromObjects(people);
    }
}
