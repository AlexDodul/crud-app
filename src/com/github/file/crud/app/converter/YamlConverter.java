package com.github.file.crud.app.converter;

import com.github.file.crud.app.models.Person;
import java.util.ArrayList;
import java.util.List;

public class YamlConverter implements Converter {

    public List<Person> fromStringDataToObjects(String file) {
        if (file == null || "".equals(file)) {
            return null;
        }
        List<Person> people = new ArrayList<>();
        long id = 0;
        String firstName = "";
        String lastName = "";
        int age = 0;
        String city = "";
        String[] arrayStr;
        String[] insideArrayStr;
        file = file.replaceAll("\r", "");
        file = file.replace("People:\n", "");
        file = file.replaceAll(" ", "");
        file = file.replaceAll("\t", "");
        arrayStr = file.split("\n");
        int j = arrayStr.length;
        for (int i = 0; i < j; i++) {
            insideArrayStr = arrayStr[i].split(":");

            int parserCode = (i + 1) % 6;
            switch (parserCode) {
                case (1):
                    continue;
                case (2):
                    id = Long.parseLong(insideArrayStr[1]);
                    continue;
                case (3):
                    firstName = insideArrayStr[1];
                    continue;
                case (4):
                    lastName = insideArrayStr[1];
                    continue;
                case (5):
                    age = Integer.parseInt(insideArrayStr[1]);
                    continue;
                case (0):
                    city = insideArrayStr[1];
                    break;
                default:
                    break;
            }
            Person yamlPerson = new Person(id, firstName, lastName, age, city);
            people.add(yamlPerson);
        }
        return people;
    }

    public String toStringDataFromObjects(List<Person> people) {
        if (people == null || people.isEmpty()) {
            return null;
        }
        String str = "People:\n";
        StringBuilder stream = new StringBuilder();
        int count = 1;
        for (Person person : people) {
            stream.append("\t- Person" + count++ + ": \n");
            stream.append("\t\tid: ").append(person.getId()).append("\n");
            stream.append("\t\tfirstName: ").append(person.getFirstName()).append("\n");
            stream.append("\t\tlastName: ").append(person.getLastName()).append("\n");
            stream.append("\t\tage: ").append(person.getAge()).append("\n");
            stream.append("\t\tcity: ").append(person.getCity()).append("\n");
        }
        return str + stream.toString();
    }
}
