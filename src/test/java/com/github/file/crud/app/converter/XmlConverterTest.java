package test.java.com.github.file.crud.app.converter;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.converter.XmlConverter;
import com.github.file.crud.app.models.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class XmlConverterTest {
    private Converter format = new XmlConverter();

    @Test
    public void toStringDataFromObjects() {
        String exp = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<people>\n\t" +
                "<person>\n\t\t<id>1</id>\n\t\t<firstName>Vasya</firstName>\n\t\t" +
                "<lastName>Pupkin</lastName>\n\t\t<age>33</age>\n\t\t<city>Odessa</city>\n\t</person>\n" +
                "</people>";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        String act = this.format.toStringDataFromObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void toStringDataFromObjectsMany() {
        String exp = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<people>\n\t" +
                "<person>\n\t\t<id>1</id>\n\t\t<firstName>Vasya</firstName>\n\t\t" +
                "<lastName>Pupkin</lastName>\n\t\t<age>33</age>\n\t\t<city>Odessa</city>\n\t</person>\n\t" +
                "<person>\n\t\t<id>2</id>\n\t\t<firstName>Masha</firstName>\n\t\t" +
                "<lastName>Korra</lastName>\n\t\t<age>23</age>\n\t\t<city>Odessa</city>\n\t</person>\n</people>";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(2L, "Masha", "Korra", 23, "Odessa"));
        String act = this.format.toStringDataFromObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void toStringDataFromObjectsNull() {
        String act = this.format.toStringDataFromObjects(null);
        Assert.assertNull(act);
    }

    @Test
    public void toStringDataFromObjectsEmpty() {
        String act = this.format.toStringDataFromObjects(new ArrayList<>());
        Assert.assertNull(act);
    }

    @Test
    public void fromStringDataToObjects() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        String data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<people>\n\t" + "<person>\n\t\t<id>1</id>\n\t\t<firstName>Vasya</firstName>\n\t\t" +
                "<lastName>Pupkin</lastName>\n\t\t<age>33</age>\n\t\t<city>Odessa</city>\n\t</person>\n" +
                "<person>\n\t\t<id>2</id>\n\t\t<firstName>Vasya</firstName>\n\t\t" + "</people>";
        List<Person> act = this.format.fromStringDataToObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsMany() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        exp.add(new Person(2L, "Masha", "Korra", 23, "Odessa"));
        exp.add(new Person(3L, "Dasha", "Koval", 13, "Odessa"));

        String data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<people>\n\t" +
                "<person>\n\t\t<id>1</id>\n\t\t<firstName>Vasya</firstName>\n\t\t" +
                "<lastName>Pupkin</lastName>\n\t\t<age>33</age>\n\t\t<city>Odessa</city>\n\t</person>\n" +
                "<person>\n\t\t<id>2</id>\n\t\t<firstName>Masha</firstName>\n\t\t" +
                "<lastName>Korra</lastName>\n\t\t<age>23</age>\n\t\t<city>Odessa</city>\n\t</person>\n" +
                "<person>\n\t\t<id>3</id>\n\t\t<firstName>Dasha</firstName>\n\t\t" +
                "<lastName>Koval</lastName>\n\t\t<age>13</age>\n\t\t<city>Odessa</city>\n\t</person>\n" +
                "</people>";
        List<Person> act = this.format.fromStringDataToObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsNull() {
        List<Person> act = this.format.fromStringDataToObjects(null);
        Assert.assertNull(act);
    }

    @Test
    public void fromStringDataToObjectsEmpty() {
        List<Person> act = this.format.fromStringDataToObjects("");
        Assert.assertNull(act);
    }
}
