package test.java.com.github.file.crud.app.converter;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.converter.JsonConverter;
import com.github.file.crud.app.models.Person;
import com.github.file.crud.app.utils.FileUtil;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class JsonConverterTest {
    private Converter format = new JsonConverter();

    @Test
    public void toStringDataFromObjects() {
        String exp = "[\n" + "\n" + "  {\n" + "    \"id\":\"1\",\n" + "    \"fname\":\"Vasya\",\n" +
                "    \"lname\":\"Pupkin\",\n" + "    \"age\":\"33\",\n" + "    \"city\":\"Odessa\"\n" + "  }\n" + "]\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        assertEquals(exp, this.format.toStringDataFromObjects(data));
    }

    @Test
    public void toStringDataFromObjectsMany() {
        String exp = "[\n" + "\n" + "  {\n" + "    \"id\":\"1\",\n" + "    \"fname\":\"Vasya\",\n" + "    \"lname\":\"Pupkin\",\n" +
                "    \"age\":\"33\",\n" + "    \"city\":\"Odessa\"\n" + "  },\n" + "  {\n" +
                "    \"id\":\"2\",\n" + "    \"fname\":\"Vasya\",\n" + "    \"lname\":\"Pupkin\",\n" +
                "    \"age\":\"33\",\n" + "    \"city\":\"Odessa\"\n" + "  }\n" + "]\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(2L, "Vasya", "Pupkin", 33, "Odessa"));
        assertEquals(exp, this.format.toStringDataFromObjects(data));
    }

    @Test
    public void toStringDataFromObjectsNull() {
        String act = this.format.toStringDataFromObjects(null);
        assertNull(act);
    }

    @Test
    public void toStringDataToObjectsEmpty() {
        assertNull(this.format.toStringDataFromObjects(new ArrayList<>()));
    }

    @Test
    public void fromStringDataToObjects() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        String data = "[\n" + "\n" + "  {\n" + "\t\"id\":\"1\",\n" + "\t\"firstName\":\"Vasya\",\n" +
                "\t\"lastName\":\"Pupkin\",\n" + "\t\"age\":\"33\",\n" + "\t\"city\":\"Odessa\"\n" + "  }\n" + "]";
        List<Person> act = this.format.fromStringDataToObjects(data);
        assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsMany() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        exp.add(new Person(2L, "Vasya", "Pupkin", 33, "Odessa"));
        exp.add(new Person(3L, "Vasya", "Pupkin", 33, "Odessa"));

        String data = "[\n" + "\n" + "  {\n" + "\t\"id\":\"1\",\n" + "\t\"firstName\":\"Vasya\",\n" + "\t\"lastName\":\"Pupkin\",\n" +
                "\t\"age\":\"33\",\n" + "\t\"city\":\"Odessa\"\n" + "  },\n" + "  {\n" +
                "\t\"id\":\"2\",\n" + "\t\"firstName\":\"Vasya\",\n" + "\t\"lastName\":\"Pupkin\",\n" + "\t\"age\":\"33\",\n" + "\t\"city\":\"Odessa\"\n" +
                "  },\n" + "  {\n" +
                "\t\"id\":\"3\",\n" + "\t\"firstName\":\"Vasya\",\n" + "\t\"lastName\":\"Pupkin\",\n" + "\t\"age\":\"33\",\n" +
                "\t\"city\":\"Odessa\"\n" + "  }\n" + "]";
        List<Person> act = this.format.fromStringDataToObjects(data);
        assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsNull() {
        assertNull(this.format.fromStringDataToObjects(null));
    }

    @Test
    public void fromStringDataToObjectsEmpty() {
        assertNull(this.format.fromStringDataToObjects(""));
    }
}