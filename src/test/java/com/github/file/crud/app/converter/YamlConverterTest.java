package test.java.com.github.file.crud.app.converter;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.converter.YamlConverter;
import com.github.file.crud.app.models.Person;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

public class YamlConverterTest {
    private Converter format = new YamlConverter();

    @Test
    public void fromStringDataToObjects() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        String data = "People:\n" + "\t-Person: " + "\n" +
                "\t\tid: " + "1" + "\n" + "\t\tfirstName: " + "Alex" + "\n" + "\t\tlastName: " + "Ddl" + "\n" +
                "\t\tage: " + "22" + "\n" + "\t\tcity: " + "Odessa" + "\n";
        List<Person> act = this.format.fromStringDataToObjects(data);
        assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsTwo() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        exp.add(new Person(2L, "Masha", "Korra", 12, "Kiev"));
        String data = "People:\n" + "\t-Person: " + "\n" +
                "\t\tid: " + "1" + "\n" + "\t\tfirstName: " + "Alex" + "\n" + "\t\tlastName: " + "Ddl" + "\n" +
                "\t\tage: " + "22" + "\n" + "\t\tcity: " + "Odessa" + "\n"
                + "\t-Person: " + "\n" +
                "\t\tid: " + "2" + "\n" + "\t\tfirstName: " + "Masha" + "\n" + "\t\tlastName: " + "Korra" + "\n" +
                "\t\tage: " + "12" + "\n" + "\t\tcity: " + "Kiev" + "\n";
        List<Person> act = this.format.fromStringDataToObjects(data);
        assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsMany() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        exp.add(new Person(2L, "Masha", "Korra", 12, "Kiev"));
        exp.add(new Person(3L, "Vasya", "Pupkin", 32, "Kiev"));
        exp.add(new Person(4L, "Dasha", "Koval", 30, "Kharkov"));
        exp.add(new Person(5L, "Anya", "Mur", 26, "Kharkov"));
        String data = "People:\n" + "\t-Person: " + "\n" +
                "\t\tid: " + "1" + "\n" + "\t\tfirstName: " + "Alex" + "\n" + "\t\tlastName: " + "Ddl" + "\n" +
                "\t\tage: " + "22" + "\n" + "\t\tcity: " + "Odessa" + "\n"
                + "\t-Person: " + "\n" +
                "\t\tid: " + "2" + "\n" + "\t\tfirstName: " + "Masha" + "\n" + "\t\tlastName: " + "Korra" + "\n" +
                "\t\tage: " + "12" + "\n" + "\t\tcity: " + "Kiev" + "\n"
                + "\t-Person: " + "\n" +
                "\t\tid: " + "3" + "\n" + "\t\tfirstName: " + "Vasya" + "\n" + "\t\tlastName: " + "Pupkin" + "\n" +
                "\t\tage: " + "32" + "\n" + "\t\tcity: " + "Kiev" + "\n"
                + "\t-Person: " + "\n" +
                "\t\tid: " + "4" + "\n" + "\t\tfirstName: " + "Dasha" + "\n" + "\t\tlastName: " + "Koval" + "\n" +
                "\t\tage: " + "30" + "\n" + "\t\tcity: " + "Kharkov" + "\n"
                + "\t-Person: " + "\n" +
                "\t\tid: " + "5" + "\n" + "\t\tfirstName: " + "Anya" + "\n" + "\t\tlastName: " + "Mur" + "\n" +
                "\t\tage: " + "26" + "\n" + "\t\tcity: " + "Kharkov" + "\n";
        List<Person> act = this.format.fromStringDataToObjects(data);
        assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsNull() {
        List<Person> act = this.format.fromStringDataToObjects(null);
        assertNull(act);
    }

    @Test
    public void fromStringDataToObjectsEmpty() {
        List<Person> exp = this.format.fromStringDataToObjects("");
        assertNull(exp);
    }

    @Test
    public void toStringDataFromObjects() {
        String exp = "People:\n" + "\t- Person1: " + "\n" +
                "\t\tid: " + "1" + "\n" + "\t\tfirstName: " + "Alex" + "\n" + "\t\tlastName: " + "Ddl" + "\n" +
                "\t\tage: " + "22" + "\n" + "\t\tcity: " + "Odessa" + "\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        String act = this.format.toStringDataFromObjects(data);
        assertEquals(exp, act);

    }

    @Test
    public void toStringDataFromObjectsTwo() {
        String exp = "People:\n" + "\t- Person1: " + "\n" +
                "\t\tid: " + "1" + "\n" + "\t\tfirstName: " + "Alex" + "\n" + "\t\tlastName: " + "Ddl" + "\n" +
                "\t\tage: " + "22" + "\n" + "\t\tcity: " + "Odessa" + "\n"
                + "\t- Person2: " + "\n" +
                "\t\tid: " + "2" + "\n" + "\t\tfirstName: " + "Masha" + "\n" + "\t\tlastName: " + "Korra" + "\n" +
                "\t\tage: " + "12" + "\n" + "\t\tcity: " + "Kiev" + "\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        data.add(new Person(2L, "Masha", "Korra", 12, "Kiev"));
        String act = this.format.toStringDataFromObjects(data);
        assertEquals(exp, act);
    }

    @Test
    public void toStringDataFromObjectsMany() {
        String exp = "People:\n" + "\t- Person1: " + "\n" +
                "\t\tid: " + "1" + "\n" + "\t\tfirstName: " + "Alex" + "\n" + "\t\tlastName: " + "Ddl" + "\n" +
                "\t\tage: " + "22" + "\n" + "\t\tcity: " + "Odessa" + "\n"
                + "\t- Person2: " + "\n" +
                "\t\tid: " + "2" + "\n" + "\t\tfirstName: " + "Masha" + "\n" + "\t\tlastName: " + "Korra" + "\n" +
                "\t\tage: " + "12" + "\n" + "\t\tcity: " + "Kiev" + "\n"
                + "\t- Person3: " + "\n" +
                "\t\tid: " + "3" + "\n" + "\t\tfirstName: " + "Vasya" + "\n" + "\t\tlastName: " + "Pupkin" + "\n" +
                "\t\tage: " + "32" + "\n" + "\t\tcity: " + "Kiev" + "\n"
                + "\t- Person4: " + "\n" +
                "\t\tid: " + "4" + "\n" + "\t\tfirstName: " + "Dasha" + "\n" + "\t\tlastName: " + "Koval" + "\n" +
                "\t\tage: " + "30" + "\n" + "\t\tcity: " + "Kharkov" + "\n"
                + "\t- Person5: " + "\n" +
                "\t\tid: " + "5" + "\n" + "\t\tfirstName: " + "Anya" + "\n" + "\t\tlastName: " + "Mur" + "\n" +
                "\t\tage: " + "26" + "\n" + "\t\tcity: " + "Kharkov" + "\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        data.add(new Person(2L, "Masha", "Korra", 12, "Kiev"));
        data.add(new Person(3L, "Vasya", "Pupkin", 32, "Kiev"));
        data.add(new Person(4L, "Dasha", "Koval", 30, "Kharkov"));
        data.add(new Person(5L, "Anya", "Mur", 26, "Kharkov"));
        String act = this.format.toStringDataFromObjects(data);
        assertEquals(exp, act);
    }

    @Test
    public void toStringDataFromObjectsNull() {
        String act = this.format.toStringDataFromObjects(null);
        assertNull(act);
    }

    @Test
    public void toStringDataFromObjectsEmpty() {
        String act = this.format.toStringDataFromObjects(new ArrayList<>());
        assertNull(act);
    }
}