package test.java.com.github.file.crud.app.converter;

import com.github.file.crud.app.converter.Converter;
import com.github.file.crud.app.converter.CsvConverter;
import com.github.file.crud.app.models.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CsvConverterTest {

    private Converter format = new CsvConverter();

    @Test
    public void toStringDataFromObjects() {
        String exp = "1,Alex,Ddl,22,Odessa\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        String act = this.format.toStringDataFromObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void toStringDataFromObjectsMany() {
        String exp = "1,Alex,Ddl,22,Odessa\n" + "2,Masha,Korra,12,Kiev\n"
                + "3,Vasya,Pupkin,32,Kiev\n" + "4,Dasha,Koval,30,Kharkov\n"
                + "5,Anya,Mur,26,Kharkov\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        data.add(new Person(2L, "Masha", "Korra", 12, "Kiev"));
        data.add(new Person(3L, "Vasya", "Pupkin", 32, "Kiev"));
        data.add(new Person(4L, "Dasha", "Koval", 30, "Kharkov"));
        data.add(new Person(5L, "Anya", "Mur", 26, "Kharkov"));
        String act = this.format.toStringDataFromObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void toStringDataFromObjectsNull() {
        String act = this.format.toStringDataFromObjects(null);
        Assert.assertNull(act);
    }

    @Test
    public void toStringDataFromObjectsEmpty() {
        String act = this.format.toStringDataFromObjects(new ArrayList<>());
        Assert.assertNull(act);
    }

    @Test
    public void fromStringDataToObjects() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        String data = "1,Alex,Ddl,22,Odessa\n";
        List<Person> act = this.format.fromStringDataToObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsMany() {
        List<Person> exp = new ArrayList<>();
        exp.add(new Person(1L, "Alex", "Ddl", 22, "Odessa"));
        exp.add(new Person(2L, "Masha", "Korra", 12, "Kiev"));
        exp.add(new Person(3L, "Vasya", "Pupkin", 32, "Kiev"));
        exp.add(new Person(4L, "Dasha", "Koval", 30, "Kharkov"));
        exp.add(new Person(5L, "Anya", "Mur", 26, "Kharkov"));
        String data = "1,Alex,Ddl,22,Odessa\n" + "2,Masha,Korra,12,Kiev\n"
                + "3,Vasya,Pupkin,32,Kiev\n" + "4,Dasha,Koval,30,Kharkov\n"
                + "5,Anya,Mur,26,Kharkov\n";
        List<Person> act = this.format.fromStringDataToObjects(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void fromStringDataToObjectsNull() {
        List<Person> act = this.format.fromStringDataToObjects(null);
        Assert.assertNull(act);
    }

    @Test
    public void fromStringDataToObjectsEmpty() {
        List<Person> act = this.format.fromStringDataToObjects("");
        Assert.assertNull(act);
    }
}
